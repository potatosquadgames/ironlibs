//
//  ISMintegralAdapter.h
//  ISMintegralAdapter
//
//  Created by Guy Lis on 04/07/2019.
//

#import <Foundation/Foundation.h>
#import "IronSource/ISBaseAdapter+Internal.h"

static NSString * const MintegralAdapterVersion = @"4.1.3";
static NSString * GitHash = @"66454182e";

//System Frameworks For Mintegral Adapter

@import Accelerate;
@import AdSupport;
@import AVFoundation;
@import CoreGraphics;
@import CoreTelephony;
@import Foundation;
@import MobileCoreServices;
@import QuartzCore;
@import StoreKit;
@import UIKit;
@import WebKit;

@interface ISMintegralAdapter : ISBaseAdapter

@end
